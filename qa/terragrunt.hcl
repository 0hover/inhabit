remote_state {
  backend = "s3"
  config = {
    bucket                  = "inhabit-coding-exercise-kw"
    key                     = "qa/terraform.tfstate"
    region                  = "eu-central-1"
    encrypt                 = true
    dynamodb_table          = "inhabitqa"
    skip_bucket_root_access = true
  }
}

terraform {
  source = "../inhabit-task"
}

inputs = {
  environment         = "qa"
  web_instance_type   = "t3.small"
  web_instance_count  = 1
  db_instance_type    = "t3.medium"
  db_instance_count   = 1
}
