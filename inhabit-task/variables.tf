variable "web_instance_count" {
  description = "Number of web server instances"
  type        = number
  default     = 1
}

variable "db_instance_count" {
  description = "Number of db server instances"
  type        = number
  default     = 1
}

variable "environment" {
  description = "Environment type indicator"
  type        = string
}

variable "web_instance_type" {
  description = "Instance type for web server"
  type        = string
}

variable "db_instance_type" {
  description = "Instance type for database server"
  type        = string
}

variable "ubuntu_2204_ami" {
  description = "AMI for Ubuntu 22.04"
  type        = string
  default     = "ami-06dd92ecc74fdfb36"
}