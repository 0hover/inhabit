resource "aws_security_group" "db_sg" {
  vpc_id = aws_vpc.main.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [aws_subnet.public.cidr_block]
  }

}

resource "aws_instance" "database_server" {
  count         = var.db_instance_count
  ami           = var.ubuntu_2204_ami
  instance_type = var.db_instance_type
  subnet_id     = aws_subnet.private.id
  security_groups = [aws_security_group.db_sg.id]

  tags = {
    Name = "database_server_${var.environment}"
  }
}