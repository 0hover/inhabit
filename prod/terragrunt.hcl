remote_state {
  backend = "s3"
  config = {
    bucket                  = "inhabit-coding-exercise-kw"
    key                     = "prod/terraform.tfstate"
    region                  = "eu-central-1"
    encrypt                 = true
    dynamodb_table          = "inhabitprod"
    skip_bucket_root_access = true
  }
}

terraform {
  source = "../inhabit-task"
}

inputs = {
  environment         = "prod"
  web_instance_type   = "t3.xlarge"
  web_instance_count  = 3
  db_instance_type    = "t3.2xlarge"
  db_instance_count   = 1
}
